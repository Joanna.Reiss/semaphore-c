#include <stdio.h>
#include <stdlib.h>
#include <semaphore.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/mman.h>

//Fonction calissone fabriquée par le cuisinier 1
//Paramètre d'entrée S : sémaphore pour assurer la transition calissone -> mielline, noyonnaise, navinette
void calissone(sem_t *S)
{
    printf("Début calissone\n");
    sleep(3 + rand() % 4); //Temps de préparation entre 3 et 6h
    printf("Fin calissone\n");
    sem_post(S); //sem_post déverouille le semaphore pointé par sem, si la valeur du semaphore est plus de zero alors un processus en sem_wait va être réveillé
    sem_post(S);
    sem_post(S);
    exit(0); // On élimine le cuisinier, après tout, seul panoramix peut connaitre la recette de la potion magique
}

//Fonction mielline fabriquée par le cuisinier 2
// S : sémaphore pour assurer la transition calissone -> mielline, noyonnaise, navinette
// CE : sémaphore de gestion de la cuillère en étain
// C : sémaphore de gestion des deux chaudrons
// P : sémaphore pour assurer la gestion fraisade, mielline, navinette -> potion
void mielline(sem_t *S, sem_t *CE, sem_t *C, sem_t *P)
{
    sem_wait(S);  //On a acquis la calissone
    sem_wait(C);  // Acquisition du chaudron
    sem_wait(CE); //  Acquisition de la cuillère en étain
    printf("Début mielline\n");
    sleep(1 + rand() % 3); // Temps de préparation entre 1 et 3h
    printf("Fin mielline\n");
    sem_post(C);  // On passe le chaudron à son voisin après l'avoir lavé
    sem_post(CE); // Idem pour la cuillère en étain
    sem_post(P);  // On se préppare pour la potion magique
    exit(0);      // Assassinat du cuisinier n°2
}

//Fonction navinette mitonnée par le cuisinier 3
void navinette(sem_t *S, sem_t *C, sem_t *P)
{
    sem_wait(S); // On a acquis la calissone
    sem_wait(C); // On acquit le chaudron
    printf("Début navinette\n");
    sleep(3 + rand() % 4); //Temps de préparation entre 3 et 6h
    printf("Fin navinette\n");
    sem_post(C); // Et on fait tourner les serv...le chaudron
    sem_post(P); // On se prépare pour la potion
    exit(0);     // Le cuisinier n°3 disparait mystérieusement...
}

// Fonction noyonnaise mijotée par le cuisinier 4
// F semaphore pour assurer la transition noyonnaise -> fraisade
void noyonnaise(sem_t *S, sem_t *C, sem_t *CE, sem_t *F)
{
    sem_wait(S);  // On obtient la calisonne
    sem_wait(C);  // On obtient le chaudron
    sem_wait(CE); // On obient la cuillère en étain
    printf("Début noyonnaise\n");
    sleep(2 + rand() % 3); // Temps de préparation entre 2 et 4h
    printf("Fin noyonnaise\n");
    sem_post(C);  // On libère le chaudron
    sem_post(CE); // On libère la cuillère
    sem_post(F);  // On se prépare pour la fraisade
    exit(0);      // Le cuisinier n°4 décède d'une intoxication alimentaire : un poisson pas frais..
}

//Fonction fraisade concoctée par le cuisinier 5
void fraisade(sem_t *F, sem_t *C, sem_t *P)
{
    sem_wait(C); //On attend le chaudron
    sem_wait(F); // On attend que la noyonnaise soit prête
    printf("Début fraisade\n");
    sleep(2 + rand() % 3); // Temps de préparation entre 2 et 4h
    printf("Fin fraisade\n");
    sem_post(C); // libération du chaudron
    sem_post(P); // On se prépare pour la potion magique
    exit(0);     // Le cuisinier s'est malheureusement pris un menhir sur la tête
}

//Fonction potion assemblée par le cuisinier 6
void potion(sem_t *P, sem_t *C, sem_t *CE)
{
    sem_wait(P); //FAIRE UN SEMAPHORE PAR INGREDIENT????
    sem_wait(P);
    sem_wait(P);
    sem_wait(C);  // On attend le chaudron
    sem_wait(CE); // On attend la cuillère en étain
    printf("Début de la potion magique\n");
    sleep(1 + rand() % 2); //Temps de préparation entre 1 et 2h
    printf("La potion magique est prête!\n");
    exit(0);
}

int main(void)
{
    int i;
    sem_t *S, *CE, *C, *P, *F;

    //Création des zones mémoires qui ne seront pas dupliquées
    S = (sem_t *)mmap(NULL, sizeof(sem_t), PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, 0, 0);
    CE = (sem_t *)mmap(NULL, sizeof(sem_t), PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, 0, 0);
    C = (sem_t *)mmap(NULL, sizeof(sem_t), PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, 0, 0);
    P = (sem_t *)mmap(NULL, sizeof(sem_t), PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, 0, 0);
    F = (sem_t *)mmap(NULL, sizeof(sem_t), PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, 0, 0);

    // Creation des sémaphores
    if (sem_init(S, 1, 0) = -1)
    {
        perror("Erreur=sem_init");
        exit(1);
    }

    if (sem_init(CE, 1, 1) = -1)
    {
        perror("Erreur=sem_init");
        exit(0);
    }

    if (sem_init(C, 1, 2) = -1)
    {
        perror("Erreur=sem_init");
        exit(0);
    }

    if (sem_init(P, 1, 1) = -1)
    {
        perror("Erreur=sem_init");
        exit(0);
    }

    if (sem_init(F, 1, 1) = -1)
    {
        perror("Erreur=sem_init");
        exit(0);
    }

    //Creation des processus fils qui sont les druides
    for (i = 0; i <= 5; i++)
    {
        if (fork() == 0)
        {
            switch (i)
            {
            case 1:
                calissone(S);
                break;
            case 2:
                mielline(S, CE, C, P);
                break;
            case 3:
                navinette(S, C, P);
                break;
            case 4:
                noyonnaise(S, C, CE, P);
                break;
            case 5:
                fraisade(F, C, P);
                break;
            case 6:
                potion(P, C, CE);
                break;
            }
        }
    }

    // Attente de Panoramix (père)
    for (i = 0; i < 6; i++)
    {
        wait(NULL);
    }

    // Destruction des sémaphores
    sem_destroy(S);
    sem_destroy(C);
    sem_destroy(CE);
    sem_destroy(P);
    sem_destroy(F);

    printf("Ils sont fous ces romains");
    exit(0);
}